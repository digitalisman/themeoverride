<?php

/***************************************************************
 * Extension Manager/Repository config file for ext "Cs.Theme".
 *
 * Auto generated 01-06-2018 22:36
 *
 * Manual updates:
 * Only the data in the array - everything else is removed by next
 * writing. "version" and "dependencies" must not be touched!
 ***************************************************************/

$EM_CONF[$_EXTKEY] = [
    'title' => 'Fluid Typo3 Theme Overrides',
    'description' => 'Digitalisman Theme by Daniel Vogel',
    'category' => 'misc',
    'shy' => 0,
    'version' => '1.1.0',
    'dependencies' => 'typo3',
    'state' => 'stable',
    'uploadfolder' => 0,
    'clearCacheOnLoad' => 1,
    'author' => 'Daniel Vogel',
    'author_email' => 'info@digitalisman.de',
    'author_company' => 'Digitalisman',
    'constraints' => [
        'depends' => [
            'typo3' => '8.7.0-9.9.99',
            'theme' => '1.1.0-1.1.99',
            'ws_scss' => '1.1.11-1.1.99'
        ],
    ],
    'autoload' => [
        'psr-4' => [
            "CS\\Themeoverride\\" => "Classes/"
        ]
    ],
];
